# OpenML dataset: chatfield_4

https://www.openml.org/d/695

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This file is a text file giving details about the time series analysed
in 'The Analysis of Time Series' by Chris Chatfield.
The 5th edn was published in 1996 and the 6th edn in 2003.
The series are listed separately at the author's home web site at
www.bath.ac.uk/~mascc/  and via the CRC Press web site at
www.crcpress.com
An individual series can readily be abstracted from this file.


Figure 11.1 - the sunspots data is available in many places but is
repeated here for convenience.
Monthly sunspots numbers from 1749 to 1983.


Note: attribute names were generated automatically since there was no
information in the data itself.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/695) of an [OpenML dataset](https://www.openml.org/d/695). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/695/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/695/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/695/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

